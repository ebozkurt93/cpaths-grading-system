export const initialForm = {
  email: 'Email',
  name: 'Ad',
  lastname: 'Soyad',
  university: 'Üniversite',
  universityYear: 'Üniversite Yılı',
  universityDept: 'Bölüm (ve varsa çift ana dal veya yan dal)',
  gpa: 'GPA',
  cv: 'CV',
  transcript: 'Transcript',
  longQuestion1:
    '2018 Haziran ayında kendini nerede görmek istiyorsun, detaylı bir şekilde açıklayabilir misin? Yurt dışında staj yapmak istiyorum veya X şirketinde çalışmak istiyorum gibi yüzeysel cevaplar maalesef başvurunu güçsüz gösterecek. Hangi spesifik alanda staj yapmak istediğinden veya X şirketinde hangi pozisyonda çalışmak istediğinden açık ve net bir şekilde bahsetmelisin :)',
  longQuestion2:
    'Bu spesifik hedefe yönelik yaptığın çalışmaları veya edindiğin tecrübeleri bizimle paylaşabilir misin? Daha önce hedefine yönelik bir çalışma yapmadıysan da hiç sorun değil, diğer soruya geçebilirsin; bu başvurunu güçsüz göstermeyecek ;)',
  longQuestion3:
    'Seni bu hedefe götüren yolu bize kısaca anlatabilir misin? Biraz ilham için: https://goo.gl/Ug74nM (Geçmiş çalışmaların veya tecrübelerin, neden bu alanlarda devam etmek istediğin/istemediğin, tanıştığın bir kişi veya ilham aldığın bir başarı hikayesinden bahsedebilirsin!)',
  longQuestion4:
    'Kesişen Yollar Danışmanlık Programı veya Kariyer Sohbetlerinden haberdar mısın? Danışmanlık Programına dahilsen veya Kariyer Sohbetlerini takip ediyorsan, ne sıklıkla takip ediyorsun? Programları faydalı buluyor musun? Bu soru başvuru değerlendirmeni etkilemeyecek, ama son bir gayret max. bir kaç dk. ayırıp bu soruyu da samimi şekilde cevaplamanı rica ediyoruz :)',
  aboutUs: "Kariyer Koçum'dan nasıl haberdar oldun?",
  accept:
    'Yukarıda vermiş olduğum bilgilerin doğruluğunu ve herhangi bir tutarsızlığın veya yanlışlığın doğurabileceği sonuçların yükümlülüğünü kabul ediyorum. Bu belgeyi tamamlayarak; Kariyer Koçum programına başvuru yaptığımın ve kabul edilmem durumunda danışan olmanın kurallarını onayladığımın farkındayım. Danışan olarak, Kesişen Yollardan herhangi bir maddi gelir beklemediğimi, oluşumla bağım devam ettiği sürece yukarıdaki bilgilerimden herhangi birinin değişiminde gerekli kişileri bilgilendireceğimi, danışmanım veya koçum ile aramdaki ilişkinin mahremiyetine saygı gösteriyor olmak ile birlikte gerekli durumlarda Kesişen Yollar platformu ile iletişime geçeceğimi onaylıyorum. Kariyer Koçum ile etkileşimim sürecinde bireysel özgürlük sınırlarını aşmayacağımı, karşı tarafın isteği olmadan herhangi bir konu üzerinde durmayacağımı, özel hayatla ilgili ve siyasi ve dini hassasiyetlere dikkat edeceğimi kabul ediyorum. Kesişen Yollar’a başvurmanın hukuki akıbetlerinin bilincindeyim: (a) Kesişen Yollar derneğini herhangi bir hukuksal açıdan dolayı sorumlu tutmayacağım. (b) Kesişen Yollar derneğine, danışan veya danışmanlarına ve koçum’a dava açmayacağım. (c) Bu programa katılmanın tüm risklerinin bilincindeyim. (d) Bu programa dahil olarak paylaştığım bilgilerin danışmanlık programı iç sosyal ağında paylaşılmasını ve diğer üyeler ve Kesişen Yollar çalışanları tarafından görülmesini onaylıyorum. (e-mail adresi gizli tutulacaktır.)'
};

export const isAFile = ['cv', 'transcript'];

export const user = {
  email: 'Email',
  name: 'İsim'
  // permissions: 'Yetkiler'
};

export const possiblePermissions = ['ADMIN', 'JURY'];

export const resultConst = {
  email: 'Email',
  name: 'Ad',
  lastname: 'Soyad',
  total_score: 'Toplam Puan'
};

export const resultPartial = {
  score: 'Puan',
  yn: 'E/H',
  notes: 'Notlar'
};

export const resultJuryPattern = [
  'id',
  'email',
  'name',
  'lastname',
  'total_score'
];
