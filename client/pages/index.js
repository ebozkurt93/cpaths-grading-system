import Nav from '../components/Nav';
import FirstForm from '../components/FirstForm';

const HomePage = props => (
  <div>
    <Nav />
    <FirstForm />
  </div>
);

export default HomePage;
